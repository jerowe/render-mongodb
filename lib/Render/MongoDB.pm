package Render::MongoDB;

use 5.010000;
use strict;
use warnings;

require Exporter;
use Carp;
use Class::MOP;
use Data::Dumper;
use Tie::IxHash;
use Moose::Role;

our @ISA = qw(Exporter);

our $VERSION = '0.01';

has mongodb => (
    isa => 'MongoDB::MongoClient',
    is => 'rw',
    lazy => 1,
    default => sub{ return MongoDB::MongoClient->new(autoconnect => 0) },
);

has mongodb_db => (
    is => 'rw',
    isa => 'MongoDB::Database',
);

has mongodb_collection => (
    is => 'rw',
    isa => 'MongoDB::Collection',
);

# Preloaded methods go here.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Render::MongoDB - MongoDB base method

=head1 SYNOPSIS

    use Moose;
    extends 'Render';
    with 'Render::MongoDB';
    with 'Render::Dgrid::MongoDB';


=head1 DESCRIPTION

    See Render::Dgrid::MongoDB  or Render::TextCSV_XS::MongoDB for full usage.

=head2 EXPORT

None by default.


=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

Jillian Rowe, E<lt>jillian.e.rowe@gmail.com<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
